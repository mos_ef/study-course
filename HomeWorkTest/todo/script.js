const form = document.getElementById('form'),
  input = document.getElementById('input'),
  todosUL = document.getElementById('todos');

  const todos = JSON.parse(localStorage.getItem('todos'));

  if (todos) {
      todos.array.forEach((todo) => addTodo(todo));
  }

  form.addEventListener('sumbit', (e) => {
      e.preventDefault();
      addTodo();
  });

  function addTodo(todo) {
      let todoText = input.value;
      console.log(input.value)
      if (todo) {
          todoText = todo.text;
      }
      if (todoText) {
          const todoEl = document.createElement('li');
          if (todo && todo.completed) {
              todo.classList.add('completed');
          }

          todoEl.innerText = todoText;

          todoEl.addEventListener('click', () => {
              todoEl.classList.toggle('completed');
              updateLS();
          });

          todoEl.addEventListener('click', (e) => {
            e.preventDefault();
            todoEl.remove();
            updateLS();
        });
        todosUL.appendChild(todoEl);
        input.value = '';
        updateLS();
      }
  }

  function updateLS() {
      todosEl = document.querySelectorAll('li');
      const todos = [];
      todosEl.forEach( (todoEl) => {
        todos.push({
            text: todoEl.innerText,
            completed: todoEl.classList.contains('completed'),
        });
      });
      localStorage.setItem('todos', JSON.stringify(todos));
  }