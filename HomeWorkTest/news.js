const news = [{
    title : 'UK trade',
    subtitle : "Ministry of Defence to pay for 'national yacht'",
    description : 'A UK business-promoting "national yacht", reportedly costing up to £200m, is to be paid for by the Ministry of Defence, Downing Street has said.',
    date : '27 Mar',
    update : '6 mins ago',
    comments : '39 comments'
}, {
    title : 'Covid in Scotland',
    subtitle : "Nicola Sturgeon defends Manchester travel ban",
    description : "Scotland's first minister has defended the ban on non-essential travel with Manchester and Salford after an angry reaction from the area's mayor.",
    date : '26 Mar',
    update : '10 mins ago',
    comments : '42 comments'  
}, {
    title : 'Billy Gilmour',
    subtitle : "Scotland midfielder tests positive for Covid-19",
    description : "Billy Gilmour has tested positive for Covid-19 and will miss Scotland's crucial Euro 2020 match with Croatia on Tuesday.",
    date : '25 Mar',
    update : '15 mins ago',
    comments : '34 comments'  
}, {
    title : 'Covid',
    subtitle : "'It took me almost all day to get my coronavirus vaccine'",
    description : "'Ellie Morgan's Covid jab appointment was just 20 minutes but actually getting the vaccine took her all day.",
    date : '25 Mar',
    update : '32 mins ago',
    comments : '31 comments'  
}, {
    title : 'Love Island 2021',
    subtitle : "ITV announces cast with first disabled contestant",
    description : "'It's been over a year since we last got to experience the thrill of coupling up, mugging off and Casa Amor chaos.'",
    date : '24 Mar',
    update : '50 mins ago',
    comments : '76 comments'  
}, {
    title : 'Covid in Scotland',
    subtitle : "Nicola Sturgeon defends Manchester travel ban",
    description : "Scotland's first minister has defended the ban on non-essential travel with Manchester and Salford after an angry reaction from the area's mayor.",
    date : '26 Mar',
    update : '10 mins ago',
    comments : '42 comments'  
}, {
    title : 'Billy Gilmour',
    subtitle : "Scotland midfielder tests positive for Covid-19",
    description : "Billy Gilmour has tested positive for Covid-19 and will miss Scotland's crucial Euro 2020 match with Croatia on Tuesday.",
    date : '25 Mar',
    update : '15 mins ago',
    comments : '34 comments'  
}, {
    title : 'Covid',
    subtitle : "'It took me almost all day to get my coronavirus vaccine'",
    description : "'Ellie Morgan's Covid jab appointment was just 20 minutes but actually getting the vaccine took her all day.",
    date : '25 Mar',
    update : '32 mins ago',
    comments : '31 comments'  
}, {
    title : 'Love Island 2021',
    subtitle : "ITV announces cast with first disabled contestant",
    description : "'It's been over a year since we last got to experience the thrill of coupling up, mugging off and Casa Amor chaos.'",
    date : '24 Mar',
    update : '50 mins ago',
    comments : '76 comments'  
}, {
    title : 'Love Island 2021',
    subtitle : "ITV announces cast with first disabled contestant",
    description : "'It's been over a year since we last got to experience the thrill of coupling up, mugging off and Casa Amor chaos.'",
    date : '24 Mar',
    update : '50 mins ago',
    comments : '76 comments'  
}];

function makeArticle(data) {
    return `<div class='normal'>
    <p class='demo-title'>Normal</p>
    <div class='module'>
        <div class='thumbnail'>
            <img
                src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/169963/photo-1429043794791-eb8f26f44081.jpeg">
            <div class='date'>
                <div>${data.date}</div> 
            </div>
        </div>
        <div class='content'>
            <div class="category">Photos</div>
            <h1 class='title'>${data.title}</h1>
            <h2 class='sub-title'>${data.subtitle}</h2>
            <div class="description">${data.description}
            </div>
            <div class="meta">
                <span class="timestamp">
                    <i class='fa fa-clock-o'></i> ${data.update}
                </span>
                <span class="comments">
                    <i class='fa fa-comments'></i>
                    <a href="#">${data.comments}</a>
                </span>
            </div>
        </div>
    </div>
</div>`
}

function renderNews(data){
    const nodeContainer = document.querySelector('.jsBlock');
    let result = '';

    data.forEach(element => {
        result += makeArticle(element);
    });

    nodeContainer.innerHTML = result;
}

setTimeout(() => {
    renderNews(news);
}, 3000)