import React, { useEffect, useState} from 'react';
import './App.css'

const makerId = () => {
  let id = 0;
  return () => id +=1
}

const getId = makerId();

 
class Api {
    constructor() {

    }

    saveTodos({value}) {
      const todo = {
        value,
        id : getId()
      }
      return todo
    }

    getTodos() {

    }

    deleteTodo() {

    }
}
const api = new Api();




const Title = ({children}) => {
 return(
   <h1>{children}</h1>
 )
}

const Input = ({...props}) => {
  return(
    <input
        className="input"
        {...props}
      />
  )
}

const Todolist = ({children}) => {
  return(
    <ul className="todos">{children}</ul>
  )
}

const TodoItem = ({children}) => {
  return( 
  <li>{children}</li>
  )
}


const App = () => {
  const [todos, setTodos] = useState([]);
  const [text, setText] = useState('');

  useEffect(() => {
    const data = api.getTodos();
    setTodos(data ? data : []);
  }, [])

  const handleSubmit = e => {
    e.preventDefault();
    if (e) {
    setText('');
    const obj = api.saveTodos({value: text});
    const newTodos = [...todos, {...obj}];
    setTodos(newTodos);
    }

  }

  const handleChange = (e) => {
    setText(e.target.value)
  }

  const handleClick = e => {
    setText('');
  }
  
  return (
    <div>
      <Title>Todos</Title>
      <form id="form" onSubmit= {handleSubmit}>
        <Input
          name = "text"
          type="text"
          onClick = {handleClick}
          onChange = {handleChange}
          id="input"
          value={text}
          placeholder="Enter your todo"
          autoComplete="off"
        />
        <Todolist>
          {todos.map(item => {
            return (
              <TodoItem key={item.id}>{item.value}</TodoItem>
            )
          })}
        </Todolist>
      </form>
    </div>
  )
}  




//_____________Вывод списка и его вложености______________________

// let data = {
//   "Рыбы": {
//     "форель": {},
//     "лосось": {}
//   },

//   "Деревья": {
//     "Огромные": {
//       "секвойя": {},
//       "дуб": {}
//     },
//     "Цветковые": {
//       "яблоня": {},
//       "магнолия": {}
//     }
//   }
// }

// const Three = ({obj}) => {
//   return (
//     <ul>
//       {Object.keys(obj).map(key => {
//         return (
//           <li>{key} <Three obj={obj[key]}/></li>
//         )
//       })}
//     </ul>
//   )
// }


// const App = () => {
//   return (
//     <div>
//       <Three obj={data}/>
//     </div>
//   )
// }




//________________________Изменение состояния_______________________________________

// const App = () => {
//   const [state, setState] = useState({ age: 19, num: 1 })

//   const handleClick = (val) =>
//     setState({
//       ...state,
//       [val]: state[val] + 1
//     })

//   const { age, num } = state
//   return (
//     <>
//       <p>Мне {age} лет.</p>
//       <p>У меня {num} братьев и сестер.</p>
//       <button onClick={() => handleClick('age')}>Стать старше!</button>
//       <button onClick={() => handleClick('num')}>
//         Больше братьев и сестер!
//       </button>
//     </>
//   )
// }

//_____________________________Споллер______________________________________________

// function Spoller({title, text}) {
//   const [show, setShow] = useState(false);
//   const toggle = () => setShow(!show)
//   return (
//     <div>
//       <button onClick={toggle} type='button'>{title}</button>
//       <div style={{ display: show ? 'block' : 'none'}}>{text}</div>
//     </div>
//   )
// }

// const App = () => {
//   return (
//     <div>
//       <Spoller title={'Spoller'} text={'text in'} />
//     </div>
//   )
// }



//_________________________________Новости___________________________________________________

// import './App.css';


// const Article = (props) => {
//   const {data} = props;
//   return (
//   <div className='normal'>
//   <p className='demo-title'>Normal</p>
//   <div className='module'>
//       <div className='thumbnail'>
//           <img
//               src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/169963/photo-1429043794791-eb8f26f44081.jpeg" />
//           <div className='date'>
//               <div>27 Mar</div> 
//           </div>
//       </div>
//       <div className='content'>
//           <div className="category">Photos</div>
//           <h1 className='title'>{data.title}</h1>
//           <h2 className='sub-title'>The city that never sleeps.</h2>
//           <div className="description">New York, the largest city in the U.S., is an architectural marvel with
//               plenty of historic monuments, magnificent buildings and countless dazzling skyscrapers.
//           </div>
//           <div className="meta">
//               <span className="timestamp">
//                   <i className='fa fa-clock-o'></i> 6 mins ago
//               </span>
//               <span className="comments">
//                   <i className='fa fa-comments'></i>
//                   <a href="#"> 39 comments</a>
//               </span>
//           </div>
//       </div>
//   </div>
// </div>
// )}

// const dataNews = [
//   {
//     id : 1,
//     title : 'UK trade',
//     subtitle : "Ministry of Defence to pay for 'national yacht'",
//     description : 'A UK business-promoting "national yacht", reportedly costing up to £200m, is to be paid for by the Ministry of Defence, Downing Street has said.',
//     date : '27 Mar',
//     update : '6 mins ago',
//     comments : '39 comments'
// }, {
//     id : 2,
//     title : 'Covid in Scotland',
//     subtitle : "Nicola Sturgeon defends Manchester travel ban",
//     description : "Scotland's first minister has defended the ban on non-essential travel with Manchester and Salford after an angry reaction from the area's mayor.",
//     date : '26 Mar',
//     update : '10 mins ago',
//     comments : '42 comments'  
// }, {
//     id : 3,
//     title : 'Billy Gilmour',
//     subtitle : "Scotland midfielder tests positive for Covid-19",
//     description : "Billy Gilmour has tested positive for Covid-19 and will miss Scotland's crucial Euro 2020 match with Croatia on Tuesday.",
//     date : '25 Mar',
//     update : '15 mins ago',
//     comments : '34 comments'  
// }, {
//     id : 4,
//     title : 'Covid',
//     subtitle : "'It took me almost all day to get my coronavirus vaccine'",
//     description : "'Ellie Morgan's Covid jab appointment was just 20 minutes but actually getting the vaccine took her all day.",
//     date : '25 Mar',
//     update : '32 mins ago',
//     comments : '31 comments'  
// },];


// const App = () => {
//   const[news, setNews] = useState([])
//   useEffect(() => {
//     setTimeout(() => {
//       setNews(dataNews)
//     }, 3000)
//   })

//   return (
//     <div>
//       <header>
//         <h1>Article News Card</h1>
//       </header>
//       <main>
//         {news.map(item => {
//           return <Article key={item.id} data={item}/>
//         })}
//       </main>
//     </div>
//   )
//   };


//_____________________Cчетчик кликов__________________________________________________________




// function Counter() {
//   const [count, setCount] = useState(10);
//   const [todos, setTodos] = useState([{ text : 'HAHAHAHPPPP'}, {text : 'LEARN React'}]);

//   return (
//     <div>
//       <ul>
//       {todos.map(item => {
//         return (
//           <li>{item.text}</li>
//         )
//       })}
//       </ul>
//       <p>Вы кликнули {count} раз</p>
//       <button onClick={() => setCount(count +1)}>Click me</button>
//     </div>
//   );
// }

// const App = () => (
// <div>
//   <Counter />
// </div>
// );

export default App;