import makeError from './makeError';

function renderError(data, errorName) {
    const nodeContainer = document.querySelector('.error-messages')
    let result = '';
    data.forEach(element => {
        result += makeError(element, errorName)
    });
    nodeContainer.innerHTML += result;
}

export default renderError;