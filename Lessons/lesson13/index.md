# FormData

В этой главе речь пойдёт об отправке HTML-форм: с файлами и без, с дополнительными полями и так далее. Объекты FormData помогут нам с этим. Как вы, наверняка, догадались по его названию, это объект, представляющий данные HTML формы.

```js
let formData = new FormData([form]);
```

Если передать в конструктор элемент HTML-формы form, то создаваемый объект автоматически прочитает из неё поля.

Его особенность заключается в том, что методы для работы с сетью, например fetch, позволяют указать объект FormData в свойстве тела запроса body.

Он будет соответствующим образом закодирован и отправлен с заголовком Content-Type: form/multipart.

То есть, для сервера это выглядит как обычная отправка формы.

# Отправка простой формы
Давайте сначала отправим простую форму.

Как вы видите, код очень компактный:

```html
<form id="formElem">
  <input type="text" name="name" value="John">
  <input type="text" name="surname" value="Smith">
  <input type="submit">
</form>

<script>
  formElem.onsubmit = async (e) => {
    e.preventDefault();

    let response = await fetch('/article/formdata/post/user', {
      method: 'POST',
      body: new FormData(formElem)
    });

    let result = await response.json();

    alert(result.message);
  };
</script>
```

Методы объекта FormData
С помощью указанных ниже методов мы можем изменять поля в объекте FormData:

- formData.append(name, value) – добавляет к объекту поле с именем name и значением value,
- formData.append(name, blob, fileName) – добавляет поле, как будто в форме имеется элемент `<input type="file">`, третий аргумент fileName устанавливает имя файла (не имя поля формы), как будто это имя из файловой системы пользователя,
- formData.delete(name) – удаляет поле с заданным именем name,
- formData.get(name) – получает значение поля с именем name,
- formData.has(name) – если существует поле с именем name, то возвращает true, иначе false

Технически форма может иметь много полей с одним и тем же именем name, поэтому несколько вызовов append добавят несколько полей с одинаковыми именами.

Поля объекта formData можно перебирать, используя цикл for..of:

```html
let formData = new FormData();
formData.append('key1', 'value1');
formData.append('key2', 'value2');

// Список пар ключ/значение
for(let [name, value] of formData) {
  alert(`${name} = ${value}`); // key1=value1, потом key2=value2
}
```

## Отправка формы с файлом

Объекты FormData всегда отсылаются с заголовком Content-Type: form/multipart, этот способ кодировки позволяет отсылать файлы. Таким образом, поля `<input type="file">` тоже отправляются, как это и происходит в случае обычной формы.

Пример такой формы:

```js
<form id="formElem">
  <input type="text" name="firstName" value="John">
  Картинка: <input type="file" name="picture" accept="image/*">
  <input type="submit">
</form>

<script>
  formElem.onsubmit = async (e) => {
    e.preventDefault();

    let response = await fetch('/article/formdata/post/user-avatar', {
      method: 'POST',
      body: new FormData(formElem)
    })
    .then(res => response.json())
    .then(data => console.log(data))
  };
</script>
```

# Зачем нужен babeljs

https://babeljs.io/repl/

JavaScript — это язык, который развивается очень быстро, и иногда мы хотим использовать его новейшие функции, но, если наш браузер или среда не позволяют этого напрямую, нам придется транспилировать его, чтобы он мог это сделать.

Транспилирование — это преобразование исходного кода, написанного на одном языке, на другой язык с сопоставимым уровнем абстракции. Следовательно, в случае JavaScript транспилятор берет синтаксис, который старые браузеры не понимают, и превращает его в синтаксис, который они понимают.

## Polyfilling vs. Transpiling

Оба метода работают с одной и той же целью: мы можем написать код, использующий новые функции, которые не реализованы в нашей целевой среде, а затем применить один из этих методов.

**Полифил** — это часть кода, в котором современные функции реализуются таким образом, чтобы их можно было применить для работы в старых версиях браузера.

**Транспилирование** — это сочетание двух слов: transforming — преобразование и compiling
— компиляция. Иногда новый синтаксис не может быть реализован с помощью полифилов, в таком случае мы используем транспилятор.

Представим, что мы используем старый браузер, который не поддерживает функцию Number.isNaN, представленную в спецификации ES6. Чтобы использовать эту функцию, нам нужно создать полифил для этого метода, но нам это понадобится только в том случае, если он еще не доступен в браузере.

```js
//Имитирует функцию isNaN
if (!Number.isNan) {//ещё не доступно.
    Number.prototype.isNaN = function isNaN(n) {
        return n !== n;
    };
}
let myNumber = 100;
console.log(myNumber.isNaN(100));
```

Мы хотим запустить следующий код в Internet Explorer 11, поэтому собираемся преобразовать его с помощью транспилятора:

```js
class mySuperClass {
  constructor(name) {
    this.name = name;
  }
hello() {
    return "Hello:" +this.name;
  }
}
const mySuperClassInstance = new mySuperClass("Rick");
console.log(mySuperClassInstance.hello()); 
//Hello Rick
```


# Зачем нужен Webpack?

Вебпак — это сборщик модулей. Он анализирует модули приложения, создает граф зависимостей, затем собирает модули в правильном порядке в один или более бандл (bundle), на который может ссылаться файл «index.html». 

![Image of вебпак](./webpack.png)

# Какие проблемы решает вебпак?

Обычно, при создании приложения на JavaScript, код разделяется на несколько частей (модулей). Затем в файле «index.html» необходимо указать ссылку на каждый скрипт. 

```html
<body>

    ...
    
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="libs/react.min.js"></script>
    <script src='src/admin.js'></script>
    <script src='src/dashboard.js'></script>
    <script src='src/api.js'></script>
    <script src='src/auth.js'></script>
    <script src='src/rickastley.js'></script>
</body>
```

Это не только утомительно, но и подвержено ошибкам. Важно не только не забыть про какой-нибудь скрипт, но и расположить их в правильном порядке. Если загрузить скрипт, зависящий от React, до загрузки самого React, приложение сломается. Вебпак решает эти задачи. Не нужно беспокоиться о последовательном включении всех скриптов. 

```html
<body>

    ...
    
    <script src='dist/bundle.js'></script>
</body>
```

## Установка вебпака

После инициализации проекта с помощью npm, для работы вебпака нужно установить два пакета — webpack и webpack-cli. 

```
npm i webpack webpack-cli -D
yarn add webpack webpack-cli -D
```

## webpack.config.js

После установки указанных пакетов, вебпак нужно настроить. Для этого создается файл `webpack.config.js`, который экспортирует объект. Этот объект содержит настройки вебпака. 

```js
module.exports = {}
```

Основной задачей вебпака является анализ модулей, их опциональное преобразование и интеллектуальное объединение в один или более бандл, поэтому вебпаку нужно знать три вещи: 

- Точка входа приложения
- Преобразования, которые необходимо выполнить
- Место, в которое следует поместить сформированный бандл

Если мы сообщим вебпаку путь до этого файла, он использует его для создания графа зависимостей приложения. Для этого необходимо добавить свойство entry в настройки вебпака со значением пути к главному файлу: 

```js
module.exports = {
    entry: './src/index.js'
}
```

## Преобразования с помощью лоадеров (loaders)

Все лоадеры включаются в массив объектов module.rules: 

Самым популярным является преобразование JavaScript следующего поколения в современный JavaScript с помощью Babel. Для этого используется babel-loader. 

```
npm i @babel/core @babel/preset-env babel-loader -D 
yarn add @babel/core @babel/preset-env babel-loader -D 
```

```js
const path = require('path')

module.exports = {
  entry: './src/index.js',
  module: {
    rules: [
      {
        test: /\.(js)$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index_bundle.js'
  }
}
```

Весь процесс выглядит примерно так: 

- Вебпак получает точку входа, находящуюся в ./src/index.js
- Он анализирует операторы import / require и создает граф зависимостей
- Вебпак начинает собирать бандл, преобразовывая код с помощью соответствующих лоадеров
- Он собирает бандл и помещает его в dist/index_bundle.js

## Режимы разработки и продакшна

Через NODE_ENV мы можем менять окружение для разработки

```
"build": "NODE_ENV='production' webpack",
"dev": "NODE_ENV='development' webpack"
```

В ключ `mode` устанавливаем нужное значение

```
mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
```
