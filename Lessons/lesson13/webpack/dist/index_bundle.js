/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/abc.js":
/*!********************!*\
  !*** ./src/abc.js ***!
  \********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"a\": () => (/* binding */ a),\n/* harmony export */   \"b\": () => (/* binding */ b)\n/* harmony export */ });\nvar a = function a() {\n  return console.log('a');\n};\n\nvar b = function b() {\n  return console.log('b');\n};\n\nvar c = function c() {\n  return console.log('c');\n};\n\nconsole.log(c);\n\n\n//# sourceURL=webpack://test-webpack/./src/abc.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _renderErrorClear__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./renderErrorClear */ \"./src/renderErrorClear.js\");\n/* harmony import */ var _renderError__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./renderError */ \"./src/renderError.js\");\n/* harmony import */ var _abc__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./abc */ \"./src/abc.js\");\n\n\n\nconsole.log(_abc__WEBPACK_IMPORTED_MODULE_2__.a, _abc__WEBPACK_IMPORTED_MODULE_2__.b, _abc__WEBPACK_IMPORTED_MODULE_2__.c);\nvar urlUser = 'https://conduit.productionready.io/api/users';\nvar nodeForm = document.querySelector('.jsSingUp');\nnodeForm.addEventListener('submit', function (e) {\n  e.preventDefault();\n  var fields = Array.from(nodeForm.querySelectorAll('input')).map(function (item) {\n    return item.getAttribute('name');\n  });\n  console.log(fields, 'fields');\n  var user = {};\n  fields.forEach(function (item) {\n    user[item] = document.querySelector(\"[name=\\\"\".concat(item, \"\\\"]\")).value;\n  });\n  fetch(urlUser, {\n    method: 'POST',\n    headers: {\n      'Content-Type': 'application/json'\n    },\n    body: JSON.stringify({\n      user: user\n    })\n  }).then(function (res) {\n    console.log(res);\n    return res.json();\n  }).then(function (data) {\n    console.log(data);\n    (0,_renderErrorClear__WEBPACK_IMPORTED_MODULE_0__.default)();\n\n    if (data.hasOwnProperty('errors')) {\n      for (var datas in data.errors) {\n        (0,_renderError__WEBPACK_IMPORTED_MODULE_1__.default)(data.errors[datas], datas);\n      }\n    }\n  })[\"catch\"](function (error) {\n    console.log('catch');\n    console.log(error);\n  });\n});\n\n//# sourceURL=webpack://test-webpack/./src/index.js?");

/***/ }),

/***/ "./src/makeError.js":
/*!**************************!*\
  !*** ./src/makeError.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\nfunction makeError(data, errorName) {\n  return \"<li>\".concat(errorName, \" \").concat(data, \"</li>\");\n}\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (makeError);\n\n//# sourceURL=webpack://test-webpack/./src/makeError.js?");

/***/ }),

/***/ "./src/renderError.js":
/*!****************************!*\
  !*** ./src/renderError.js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _makeError__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./makeError */ \"./src/makeError.js\");\n\n\nfunction renderError(data, errorName) {\n  var nodeContainer = document.querySelector('.error-messages');\n  var result = '';\n  data.forEach(function (element) {\n    result += (0,_makeError__WEBPACK_IMPORTED_MODULE_0__.default)(element, errorName);\n  });\n  nodeContainer.innerHTML += result;\n}\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (renderError);\n\n//# sourceURL=webpack://test-webpack/./src/renderError.js?");

/***/ }),

/***/ "./src/renderErrorClear.js":
/*!*********************************!*\
  !*** ./src/renderErrorClear.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\nfunction renderErrorClear() {\n  var nodeErrorClear = document.querySelector('.error-messages');\n  var result = '';\n  nodeErrorClear.innerHTML = result;\n}\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (renderErrorClear);\n\n//# sourceURL=webpack://test-webpack/./src/renderErrorClear.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.js");
/******/ 	
/******/ })()
;