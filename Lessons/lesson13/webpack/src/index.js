import renderErrorClear from './renderErrorClear';
import renderError from './renderError';
import { a, b, c } from './abc';

console.log(a, b, c)

const urlUser = 'https://conduit.productionready.io/api/users'
const nodeForm = document.querySelector('.jsSingUp')
nodeForm.addEventListener('submit', function (e) {
    e.preventDefault();
    const fields = Array.from(nodeForm.querySelectorAll('input')).map(item => item.getAttribute('name'));
    console.log(fields, 'fields')
    const user = {};
    fields.forEach(item => {
        user[item] = document.querySelector(`[name="${item}"]`).value;
    });
    fetch(urlUser, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ user })
    })
        .then(res => {
            console.log(res)
            return res.json()
        })
        .then(data => {
            console.log(data)
            renderErrorClear()
            if (data.hasOwnProperty('errors')) {
                for (let datas in data.errors) {
                    renderError(data.errors[datas], datas)
                }
            }
        })
        .catch(error => {
            console.log('catch')
            console.log(error)
        })
})