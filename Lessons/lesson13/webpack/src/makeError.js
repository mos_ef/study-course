function makeError(data, errorName) {
    return `<li>${errorName} ${data}</li>`
}

export default makeError;