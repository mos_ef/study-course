import { createStoreon } from 'storeon'


let userState = store => {
    store.on('@init', () => ({adminStatus: false}))
    store.on('changeStatus', ({adminStatus}) => ({adminStatus: !adminStatus}))
    store.on('getStatus', ({adminStatus}) => {return adminStatus})
}

export const store = createStoreon([userState]);