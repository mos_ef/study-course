import './App.css';
import {BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from './components/Header/Header';
import SignIn from './components/Header/SignIn/SignIn';
import SignUp from './components/Header/SignUp/SignUp';
import {PATHS} from './components/Header/Header'
import Home from './components/Home/Home';
import UserPage from './components/Home/UserPage/UserPage'
import ArticlePage from './components/Home/ArticlePage/ArticlePage' 
import {store} from './state/state'
import Settings from './components/AuthPages/SettingPage/SettingPage';
import NewPostPage from './components/AuthPages/NewPostPage/NewPostPage';
import { useStoreon } from 'storeon/react'

function App() {
  const { dispatch, adminStatus } = useStoreon('adminStatus')
  const isAdmin = store.get()
  return (
    <BrowserRouter>
      <Header />
      <Route exact component={Home} path={PATHS.home.url}/>
      {!isAdmin &&  
        <Switch>
          <Route exact  component={SignUp} path={PATHS.signUp.url}/>
          <Route exact component={SignIn} path={PATHS.signIn.url}/>
        </Switch>
      }
      {isAdmin &&  
        <Switch>
          <Route component={NewPostPage} path={PATHS.new_post.url}/>
          <Route component={Settings} path={PATHS.settings.url}/>
        </Switch>}

      <Route component={ArticlePage} path={PATHS.article_user.url + ':slug'} />
      <Route component={UserPage} path={PATHS.profile_user.url + ':username'} />
    </BrowserRouter>
  );
}

export default App;

