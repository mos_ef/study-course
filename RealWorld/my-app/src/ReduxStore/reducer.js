import {applyMiddleware, combineReducers, createStore} from 'redux'
import {composeWithDevTools} from 'redux-devtools-extension'
import thunk from 'redux-thunk'




const SET_USER = 'SET_USER'
const LOGOUT = 'LOGOUT'

const defaultState = {
    currentUser : {},
    isAuth: false,
    username: ''
}

export default function userReducer (state = defaultState, action){
    switch(action.type){
        case SET_USER:
            localStorage.setItem('username', action.payload.username)
            return {
                ...state,
                currentUser: action.payload,
                isAuth: true,
                username : action.payload.username
            }
        case LOGOUT:
            localStorage.removeItem('token')
            return {
                ...state,
                currentUser: {},
                isAuth: false,
            }
        default:
            return state
    }
}



const rootReducer = combineReducers({
    user: userReducer
})



export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))
export const setUser = user => ({type: SET_USER, payload: user})
export const userLogout = () => ({type: LOGOUT})
