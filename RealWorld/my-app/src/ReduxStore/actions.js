import { setUser } from "../reducers/userReducer"

export let errorArr = []

export const login = (email,password) => {
        return async dispatch => {
        let user = {email : email , password: password}
        const urlLogin = 'https://conduit.productionready.io/api/users/login'
        const data = await fetch(urlLogin,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include',
            body: JSON.stringify({user})
        })
        let res = await data.json()
        console.log(res)
        console.log(res.user)
        if(res.hasOwnProperty('errors')){
            console.log(res.errors)
            for (let results in res.errors){
            res.errors[results].map(text => errorArr.push([results,' ', text]))
            console.log(errorArr)
            }
        } else {
            dispatch(setUser(res.user))
            localStorage.setItem('token', res.user.token);
    }
}        
}

