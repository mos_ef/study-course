import { useState } from "react";
import { Input, Button } from "../../Header/InputForm/InputForm";


const NewPostPage = () => {
    
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [body, setBody] = useState('')
    const [tagList, setTagList] = useState([])

    const handleSubmit = async (e) => {
        e.preventDefault();
        let article = {title: title, description : description , body: body, tagList: tagList}
        const urlNewPost = 'http://testdjango.staging.garpix.com/api/articles'
            await fetch(urlNewPost,{
                method: 'POST',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({article})
            })
        }

    return (
        <div className="container page">
    <div className="row">
        <div className="col-md-10 offset-md-1 col-xs-12">
            <form onSubmit={handleSubmit}>
                <fieldset>
                    <Input type="text" placeholder="Article Title" value={title} onChange={(e) => setTitle(e.target.value)}/>
                    <Input type="text" placeholder="What's this article about?" value={description} onChange={(e) => setDescription(e.target.value)}/>
                    <Input type="text" rows='8' placeholder="Write your article (in markdown)" value={body} onChange={(e) => setBody(e.target.value)}/>
                    <Input type="text" placeholder="Enter tags"  value={tagList} onChange={(e) => setTagList(e.target.value)}/>
                    <Button text='Publish Article'/>
                </fieldset>
            </form>
        </div>
    </div>
</div>
    )
}

export default NewPostPage
