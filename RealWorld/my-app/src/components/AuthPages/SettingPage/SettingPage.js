import {Button, Input} from '../../Header/InputForm/InputForm';
import {store} from '../../../state/state'
import { useStoreon } from 'storeon/react'

const Settings = () => {
    
    const { dispatch, adminStatus } = useStoreon('adminStatus')
    const st = store.get()
    return (
        

        <div>
        <div className="settings-page">
            <div className="container page">
                <div className="row">
                    <div className="col-md-6 offset-md-3 col-xs-12">
                        <h1 className="text-xs-center">Your Settings</h1>
                        <form>
                            <fieldset>
                                <Input type="text" placeholder="URL of profile picture"/>
                                <Input type="text" placeholder="Username"/>
                                <Input type="text" placeholder="Short bio about you"/>
                                <Input type="text" placeholder="Email"/>
                                <Input type="text" placeholder="New Password"/>
                                <Button text='Update Settings' />
                            </fieldset>
                        </form>
                        <hr /><button className="btn btn-outline-danger" onClick={() => dispatch('changeStatus')}>Or click here to logout.</button>
                        <hr /><button  onClick={() => console.log(st)}>click </button>
                    </div>
                </div>
            </div>
        </div>
        </div>
    )
}

export default Settings
