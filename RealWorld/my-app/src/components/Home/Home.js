
import Banner from "./Banner/Banner";
import Feed from "./Feed/Feed";


const Home = () => {
    return (
        <div className='home-page'>
            <Banner />
            <Feed />
        </div>

    )
}

export default Home;