import { useState, useEffect } from 'react';
import Tag from './Tag/Tag';
import { generateUUID } from '../Api/Api';

const PopularTags = (props) =>{

  const [tags, setTags] = useState([])

  useEffect(() => {
    (async ()=>{
        let data = await fetch('https://conduit.productionready.io/api/tags')
        let res = await data.json()
        // console.log(res.tags)
        setTags(res.tags)
    })()
}, [] )

  const Tags = tags.map(t => <Tag key={generateUUID()} title={t}/>)

    return(
              <div className="sidebar">
                <p>Popular Tags</p>
                <div id="tag-list" className="tag-list">
                  {Tags}
                </div>
              </div>
    )
}

export default PopularTags
