import FeedToggle from '../Feed/FeedToggle/FeedToggle'
import { useState, useEffect } from 'react';
import Article from '../../Home/Feed/Article/Article'
import UserBanner from './UserBanner/UserBanner';

const UserPage = (props) => {

    const [userArticles, setArticles] = useState([])
    const [userData, setData] = useState({})
    

    useEffect(() => {
        (async ()=>{
            let Articles = await fetch('https://conduit.productionready.io/api/articles?author=' + props.match.params.username)
            let Data = await fetch('https://conduit.productionready.io/api/profiles/' + props.match.params.username)
            let resArticles = await Articles.json()
            let resData = await Data.json();
            console.log(resArticles)
            console.log(resData)
            setArticles(resArticles.articles)
            setData(resData.profile)
        })()
    }, [] )

    const Articles = userArticles.map( a => <Article state={a}/>)


    return(
        <div className="profile-page">
                    <UserBanner userData={userData}/>
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-md-10 offset-md-1">
                                <FeedToggle />
                                {Articles}
                            </div>
                        </div>
                    </div>
                </div>

    )
}

export default UserPage