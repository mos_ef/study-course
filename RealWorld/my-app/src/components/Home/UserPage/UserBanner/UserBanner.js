const UserBanner = ({userData}) => {
    
    return(
        <div className="user-info">
        <div className="container">
            <div className="row">
                <div className="col-xs-12 col-md-10 offset-md-1"><img src={userData.image} className="user-img" alt='user-image'/>
                    <h4>{userData.username}</h4>
                    <p></p>
                    <button className="btn btn-sm action-btn btn-outline-secondary">
                        <i className="ion-plus-round"></i>&nbsp;
                        {userData.following ? `Unfollow ${userData.username}` : `Follow ${userData.username}`}
                    </button>
                </div>
            </div>
        </div>
    </div>
    )
}

export default UserBanner
