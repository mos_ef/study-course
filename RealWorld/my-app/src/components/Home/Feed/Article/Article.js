import { NavLink } from 'react-router-dom';
import LikeButton from './LikeButton/LikeButton';
import RenderTag from './RenderTags/RenderTags';
import { generateUUID } from '../../Api/Api';
import { PATHS } from '../../../Header/Header';

const Article = ({state}) => {
    const taglist1 = state.tagList;
    const taglist_copy = taglist1.map( el => <RenderTag key={generateUUID()} elem={el}/> )

    const time = Date.parse(state.createdAt);
    const date = new Date(time);

    return (
        <div className="article-preview">
            <div className="article-meta">
                <NavLink to={PATHS.profile_user.url + state.author.username}>
                    <img src={state.author.image} alt='user-img'/>
                </NavLink>
                <div className="info">
                <NavLink className="author" to={PATHS.profile_user.url + state.author.username}>
                        {state.author.username}
                </NavLink>
                <span className="date">
                    {date.toDateString()}
                </span>
                </div>
                <LikeButton likeCount={state.favoritesCount}/>
            </div>
            <NavLink to={PATHS.article_user.url + state.slug} className="preview-link">
                <h1>{state.title}</h1>
                <p>{state.description}</p>
                <span>Read more...</span>
                <ul className="tag-list">
                    {taglist_copy}
                </ul>
            </NavLink>
        </div>
    )
}

export default Article;
