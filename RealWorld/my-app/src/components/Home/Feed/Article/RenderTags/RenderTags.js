

const RenderTag = ({elem}) => {
    return (
        <>
        <li className="tag-default tag-pill tag-outline">
            {elem}
        </li>
        </>
    )
}

export default RenderTag;