const LikeButton = ({ likeCount }) => {
    return (
      <div className="pull-xs-right">
        <button className="btn btn-sm btn-outline-primary" type="button">
          <i className="ion-heart"></i>
          {likeCount}
        </button>
      </div>
    );
  };
  
  export default LikeButton;
  