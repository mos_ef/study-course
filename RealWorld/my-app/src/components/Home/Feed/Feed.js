import { useState, useEffect } from 'react';
import FeedToggle from './FeedToggle/FeedToggle';
import Article from './Article/Article';
import PopularTags from '../PopularTags/PopularTags';
import PanelNavigation from '../PanelNavigation/PanelNavigation';


const Feed = () => {
    const [news, setNews] = useState([])
    const [offset, setOffset] = useState(0)
    
    useEffect(() => {
        (async () => { 
            let data = await fetch('https://conduit.productionready.io/api/articles?limit=10&offset=' + String(offset))
            let res = await data.json()
            // console.log(res.articles)
            setNews(res.articles)
        })()    
    }, [offset] )

    const articles = news.map( art => <Article key={art.slug} state={art}/>)
    

    
    const paginate = function(offset) {
        setOffset(offset)
    }

    return (
        <div className="container page">
            <div className="row">
                <div className="col-md-9">
                    <FeedToggle/>
                    <>
                    {articles}
                    </>
                    <PanelNavigation prop = {paginate} />
                </div>
                <div className="col-md-3">
                    <PopularTags />
                </div>
            </div>
        </div>
    )
}

export default Feed;
