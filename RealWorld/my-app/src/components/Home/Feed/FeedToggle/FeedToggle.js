import { NavLink } from "react-router-dom";
import {PATHS} from "../../../Header/Header"

const FeedToggle = () => {
    return(
        <>
        <div className="feed-toggle">
                <ul className="nav nav-pills outline-active">
                   <li className="nav-item">
                      <NavLink to={PATHS.home.url}  className="nav-link active">Global feed</NavLink>
                   </li>
                </ul>
              </div>
        </>
        )
}

export default FeedToggle;
