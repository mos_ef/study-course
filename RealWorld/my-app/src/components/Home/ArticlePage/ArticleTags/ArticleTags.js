

const ArticleTags = (props) => {
    return(
        <li className="tag-default tag-pill tag-outline">
            {props.title}
        </li>    
        )
}

export default ArticleTags;
