import { useState, useEffect } from 'react';
import ArticleBanner from './ArticleBanner/ArticleBanner';
import ArticleBody from './ArticleBody/ArticleBody';


const ArticlePage = (props) => {
    const [data, setData] = useState({});
    
    useEffect(() => {
        (async () => { 
            const articleUrl = ('https://conduit.productionready.io/api/articles/' + props.match.params.slug)
            let data = await fetch(articleUrl)
            let res = await data.json()
            setData(res)
        })()    
    }, [] )

    function isEmptyObject(obj) {
    for (let i in obj) {
        if (obj.hasOwnProperty(i)) {
            return true;
        }
    }
    return false;
}
 
    function AAA () {
        console.log(data)
        if (isEmptyObject(data)) return (<ArticleBanner props={data.article}/>)
    }

    function BBB () {
        console.log(data)
        if (isEmptyObject(data)) return (<ArticleBody props={data.article}/>)
    }

    return (
        <div className="arcticle-page">
            {AAA()}
            {BBB()}
        </div>
    )


}

export default ArticlePage;