import { NavLink } from "react-router-dom";
import { PATHS } from "../../../Header/Header";
import { useState, useEffect } from "react";


const ArticleBanner = ({props}) => {


    const time = Date.parse(props.createdAt);
    const date = new Date(time);

    return (    
        <div className="banner">
            <div className="container">
                <h1>{props.title}</h1>
                <div className="article-meta">
                    <NavLink to={PATHS.profile_user.url + props.author.username}>
                        <img src={props.author.image} alt='user-img'/>
                    </NavLink>
                  <div className="info">
                    <NavLink className="author" to={PATHS.profile_user.url + props.author.username}>
                        {props.author.username}
                    </NavLink>
                    <span className="date">{date.toDateString()}</span>
                </div>
                <span></span>
                </div>
            </div>

        </div>
    )   
}

export default ArticleBanner;

