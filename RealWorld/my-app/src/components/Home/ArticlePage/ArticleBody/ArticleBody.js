import { NavLink } from "react-router-dom";
import { generateUUID } from "../../Api/Api"
import ArticleTags from "../ArticleTags/ArticleTags"
import { PATHS } from "../../../Header/Header";


const ArticleBody = ({props}) => {
    const tagList1 = props.tagList;
    const Tags = tagList1.map(t => <ArticleTags key={generateUUID()} title={t}/>)
    console.log(props)

    return (
        <div className="container page">
            <div className="row article-content">
                <div className="col-xs-12">
                    <div>
                        <p>{props.body}</p>
                    </div>
                    <ul className="tag-list">
                        {Tags}
                    </ul>
                </div>
            </div>
            <hr/>
            <div className="article-actions"></div>
            <div className="row">
                <div className="col-xs-12 col-md-8 offset-md-2">
                    <p>
                        <NavLink to={PATHS.signIn.url} >Sign in</NavLink>
                    &nbsp;or&nbsp;
                    <NavLink to={PATHS.signUp.url} >Sign up</NavLink>
                   &nbsp;to add comments on this article.
                   </p>
                </div>
            </div>
        </div>
    )
}

export default ArticleBody;