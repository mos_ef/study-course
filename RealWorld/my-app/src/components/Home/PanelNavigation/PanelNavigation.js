import { useState } from "react";
import { NavLink } from "react-router-dom";




const PanelNavigation = (prop) => {
    
    const mass = []
    for ( let i = 1; i<=50; i++) {
        mass.push(i)
    }
    
    const render = mass.map(el => <li className="page-item" key={el} onClick={() => prop.prop((el * 10) - 10)}>
    <NavLink to='/' className="page-link" >
        {el}
    </NavLink>
    </li>
    )


    return(
        <nav>
            <ul className='pagination'>
            { render }
            </ul>
        </nav>
        )
}

export default PanelNavigation
