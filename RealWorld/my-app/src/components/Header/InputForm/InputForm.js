const Input = ({...props}) => {
    return(
        <fieldset className="form-group">
        <input
            className="form-control form-control-lg"
            {...props}
        />
        </fieldset>
        )
}

const Button = ({text}) =>{
    return(
        <button 
            className="btn btn-lg btn-primary pull-xs-right" 
            type="submit"
            >
            {text}
        </button>
        )
}


export {
    Input,
    Button
};
