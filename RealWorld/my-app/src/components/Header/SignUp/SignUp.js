import { NavLink } from 'react-router-dom';
import { PATHS } from ".././Header";
import RegisterForm from './RegisterForm/RegisterForm';

const SignUp = () => {
    return(
        <>
            <div className="auth-page">
                <div className="container page">
                    <div className="row">
                        <div className="col-md-6 offset-md-3 col-xs-12">
                            <h1 className="text-xs-center">Sign Up</h1>
                            <p className="text-xs-center">
                                <NavLink to={PATHS.signIn.url}>Have an account?</NavLink>
                            </p>
                            <RegisterForm />
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}

export default SignUp;