
const RegisterError = (props) =>{
    const error = props.props.map(err => <li>{err}</li>) 

    return(
        <ul className="error-messages">
            {error}
        </ul>   
        )
}

export default RegisterError;
