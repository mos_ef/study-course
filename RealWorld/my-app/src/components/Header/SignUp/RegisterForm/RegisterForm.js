import { useState } from 'react';
import {Button, Input} from '../../InputForm/InputForm';
import RegisterError from '../RegisterError/RegisterError';

const RegisterForm = () => {

    const [username, setUser] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [errors, setErrors] = useState([])

    const handleSubmit = async (e) => {
        console.log('отправка')
        e.preventDefault()
        let user = {username: username, email : email , password: password}
        const data = await fetch('https://conduit.productionready.io/api/users',{
          method: 'POST',
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({user})
      })
         let callback = await data.json()
         console.log(callback)
         let errorArr = []
         if(callback.hasOwnProperty('errors')){
            for (let el in callback.errors){
                callback.errors[el].map(text => errorArr.push([el,' ', text]))
            }   
             setErrors(errorArr)
        } else alert('Регистрация успешно выполненна')
    }

    return (
        <>
            <RegisterError props={errors}/>
            <form onSubmit={handleSubmit} >
                <fieldset >
                    <Input value={username}  type='text' placeholder='Username'onChange={(e) => setUser(e.target.value)} />
                    <Input value={email} type='email' placeholder='Email' onChange={(e) => setEmail(e.target.value)} />
                    <Input value={password} type='password' placeholder='Password' onChange={(e) => setPassword(e.target.value)} />
                    <Button text='Sign Up' />
                </fieldset>
            </form>
        </>
    )

}

export default RegisterForm