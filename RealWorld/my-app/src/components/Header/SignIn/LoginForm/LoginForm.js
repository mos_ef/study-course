import { useState } from 'react';
import {Button, Input} from '../../InputForm/InputForm';
import LoginError from '../LoginError/LoginError';
import { useStoreon } from 'storeon/react'
import {store} from '../../../../state/state'

const LoginForm = () => {
    const { dispatch, adminStatus } = useStoreon('adminStatus')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [errors, setErrors] = useState([])

    const handleSubmit = async (e) => {
        console.log('отправка')
        e.preventDefault();
        let user = {email : email , password: password}
        const data = await fetch('https://conduit.productionready.io/api/users/login',{
          method: 'POST',
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({user})
      })
          let callback = await data.json()
          console.log(callback)
          if(callback.hasOwnProperty('errors')){
          let errorArr = []
          for (let res in callback.errors){
            callback.errors[res].map(text => errorArr.push([res,' ', text]))
      }
      setErrors(errorArr)
    } else{
        setEmail(callback.user)
        localStorage.setItem('token', callback.user.token)
        store.dispatch('changeStatus')
    }
}
     
    
    return (
        <>
            <LoginError props={errors} />
            <form onSubmit={handleSubmit}>
                <fieldset>
                    <Input value={email} type='email' placeholder='Email' onChange={(e) => setEmail(e.target.value)}/>
                    <Input value={password} type='password' placeholder='Password' onChange={(e) => setPassword(e.target.value)}/>
                    <Button text='Sign Ip' />
                </fieldset>
            </form>
        </>
    )

}

export default LoginForm;