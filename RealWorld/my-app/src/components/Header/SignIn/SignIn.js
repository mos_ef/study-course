import { NavLink } from "react-router-dom";
import LoginForm from "./LoginForm/LoginForm";
import { PATHS} from ".././Header";

const SignIn = () => {



  return (
    <div className="auth-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-6 offset-md-3 col-xs-12">
            <h1 className="text-xs-center">Sign In</h1>
            <p className="text-xs-center">
              <NavLink to={PATHS.signUp.url}>Need an account?</NavLink>
            </p>
            <LoginForm />
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignIn;
