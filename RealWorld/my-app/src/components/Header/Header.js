import { NavLink } from "react-router-dom";
import {store} from '../../state/state'
import { useStoreon } from 'storeon/react'

const PATHS = {
    home: {
      url: '/'
    },
    signIn: {
      url: '/login'
    },
    signUp: {
      url: '/register'
    },
    profile_user : {
      url: '/profile/'
    },
    article_user : {
      url : '/article/'
    },
    new_post : {
      url : '/new_post/'
    },
    settings : {
      url : '/settings/'
    }
  }


const Header = () => {
  const { dispatch, adminStatus } = useStoreon('adminStatus')
  const username = 'udav154'
  const isAuth = store.get()


    return(
        <>
            <nav className="navbar navbar-light">
                <div className="container">
                    <NavLink exact to={PATHS.home.url} className="navbar-brand">conduit</NavLink>
                    <ul className="nav navbar-nav pull-xs-right">
                        <li className="nav-item">
                          <NavLink exact to={PATHS.home.url} className="nav-link">Home</NavLink>
                        </li>
                        {!isAuth && 
                        <li className="nav-item">
                            <NavLink to={PATHS.signIn.url} className='nav-link'>Sign in</NavLink>
                        </li>}
                        {!isAuth && 
                        <li className="nav-item">
                            <NavLink to={PATHS.signUp.url} className='nav-link'>Sign up</NavLink>
                        </li>}
                        {isAuth && 
                        <li className="nav-item">
                            <NavLink to={PATHS.new_post.url} className='nav-link'>New Post</NavLink>
                        </li>}
                        {isAuth && 
                        <li className="nav-item">
                            <NavLink to={PATHS.settings.url} className='nav-link'>Settings</NavLink>
                        </li>
                        }
                        {isAuth && 
                        <li className="nav-item">
                            <NavLink to={PATHS.profile_user.url + username} className='nav-link'>{username}</NavLink>
                        </li>
                        }
                    </ul>
                </div>
            </nav>
        </>

    )
}

export default Header;
export {PATHS};